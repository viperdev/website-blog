# VIPERdev Blog Site

*A Site Starter Materialize Templates.

The theme is a fork of the [materialize][4]

The website preview is available at: http://viperdev-new.netlify.com/


# License

The theme is available as open source under the terms of the [MIT License][2].

[Materialize][3] a  modern responsive front-end framework based on Material Design

Copyright © 2019 VIPERdev. Powered by <a href="http://jekyllrb.com">Jekyll</a>

[1]: https://github.com/jekyll/minima
[2]: https://opensource.org/licenses/MIT
[3]: http://materializecss.com/
[4]: https://github.com/macrod68/jekyll-materialize-starter-template
