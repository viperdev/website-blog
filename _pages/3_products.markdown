---
layout: cta-page
title: Products
permalink: /products/
lang: en
---

## MVP Product Development

{% include products_en.html %}

## Dedicated Team

Planning to release multiple MVPs or struggling to get your ongoing development
done fast? We can help you by assembling a custom team to build your own MVP
factory or boost your MVP to a quick release.

<a href="/startproject" class="btn-large">Get More Information</a>

## Workshops/Consulting

If you want to iterate on your business model or start changing the culture in
your company, we can help you by consulting your and your team.

<a href="/startproject" class="btn-large">Get More Information</a>

## How We Work with You

{% for proc in site.data.process_en %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ proc.heading }}</h3>
    <p>{{ proc.text }}</p>
  </div>
</div>
{% endfor %}
