---
layout: page
permalink: /startproject/
title: Projekt Starten
lang: de
---

Erzähle uns mehr von Deinem Projekt/Workshop indem Du dieses Formular
ausfüllst - wir melden uns schnellstmöglich:

<form name="startproject" method="POST" netlify>
  <div class="input-field">
    <label for="name">Name</label><input type="text" name="name" id="name"/>
  </div>
  <div class="input-field">
    <label for="email">Email</label><input type="text" name="email" id="email"/>
  </div>
  <div class="input-field">
    <label for="phone">Telefon</label><input type="text" name="phone" id="phone"/>
  </div>
  <div class="input-field">
    <label for="project">Projektbeschreibung/Workshopthema (Optional)</label>
    <textarea name="project" id="project" class="materialize-textarea"></textarea>
  </div>
  <div class="input-field">
    <label for="source">Wie wurdest Du auf uns aufmerksam? (Optional)</label>
    <input type="text" name="source" id="source"/>
  </div>
  <p>Mit dem Abschicken des Formulares erklärst Du Dich mit der Verarbeitung Deiner Daten durch uns einverstanden. Mehr Informationen dazu findest Du in unserer <a href="/datenschutzerklaerung">Datenschutzerklärung</a>.</p>
  <button type="submit" class="btn-large">Abschicken</button>
</form>

# Produktüberblick

{% include products_de.html %}
