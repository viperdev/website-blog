---
layout: cta-page
title: Produkte
permalink: /products/
lang: de
---

## MVP Produktentwicklung

{% include products_de.html %}

## VIPERdev Teams

Mehrere MVPs in planung oder Probleme, ein eigenes Produkt schnell auf die
Strasse zu bringen? Wir helfen Euch, eure eigene MVP Fabrik aufzubauen und
unterstützen euer Team, euer Produkt schnell zu releasen.

<a href="/startproject" class="btn-large">Mehr Informationen Anfragen</a>

## Workshops/Consulting

Wir helfen Kunden, ihr Business Modell zu optimieren und die ideale
Markteintrittsstrategie zu ermitteln. Dies geschieht entweder im One-on-One
Consulting oder in einem Workshop mit den relevanten Teammitgliedern.

<a href="/startproject" class="btn-large">Mehr Informationen Anfragen</a>

## Wie Wir mit Dir Arbeiten

{% for proc in site.data.process_de %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ proc.heading }}</h3>
    <p>{{ proc.text }}</p>
  </div>
</div>
{% endfor %}
