---
layout: default
title: Startseite
permalink: /
lang: de
---

<div id="titlecover">
  <div class="title-carousel">
    <div class="carousel-item" style="background-image:url(../assets/img/home/ladder.jpg)"></div>
  </div>
	<div class="title-slogan">
		<div class="slanted">
			<h1><span>Dein Softwareprodukt in 8&nbsp;Wochen.</span></h1>
		</div>
		<div>
			<h2><span>Lass&nbsp;Dich&nbsp;nicht&nbsp;verarschen! Wir&nbsp;bauen&nbsp;Software die&nbsp;Du&nbsp;wirklich&nbsp;brauchst.</span></h2>
		</div>
	</div>
	<div class="title-bottom">
		<div class="social-links" id="social-media-links">
  		<ul>
  			<li><a href="https://twitter.com/viperdev_io" target="_blank" class="twitter"></a></li>
  			<li><a href="https://www.linkedin.com/company/viperdev/" target="_blank" class="linkedin"></a></li>
  			<li><a href="/startproject" target="_blank" class="envelope"></a></li>
  			<li><a href="/feed.xml" target="_blank" class="rss" title="zum Newsletter anmelden"></a></li>
  		</ul>
  	</div>
		<a href="#site-header" id="scroll-to-content"></a>
	</div>
</div>

<div class="container section center">
  <h3>
    Bring Deine Idee in 8 Wochen an den Markt.&nbsp;&nbsp;
    <a  href="/products/" class="btn-large">UNSERE PRODUKTE</a>
  </h3>
</div>

<div class="stripe z-depth-1">
  <div class="container section row">
    <div class="col s12 m3">
      <a class="colorless" href="/about">
        <img class="viperdev_img center" src="/assets/img/viperdev_footer.png" alt="VIPERdev Logo">
      </a>
    </div>
    <div class="col s12 m9">
    <p style="text-align:justify">
      <a class="colorless" href="/about">
      VIPERdev ist ein Startup für Software-Entwicklung aus Hamburg. Wir sind Entwickler. Aber wir sind auch erfahrene Gründer. Aus diesem Grund stellen wir immer zuerst die Bedürfnisse unserer Kunden in den Mittelpunkt - und nicht den Kostenaufwand.
      Gemeinsam finden wir heraus, was Du wirklich willst. Gleichzeitig setzen wir Deine Software-Ideen schnell und kostengünstig in konkrete Produkte um.
      </a>
    </p>
    </div>
  </div>
</div>


  <div class="section container">
    <h3>
      Erfolgsgeschichten
    </h3>
    <div class="carousel-class">
			{% for story in site.data.success_de %}
      <div data-title="{{ story.short_title }}">
        <div class="row">
          <div class="col s12 m6">
            <h4>{{ story.title }}</h4>
            <blockquote><p>{{ story.text }}</p></blockquote>
            <div class="author">
							<a href="{{ story.author_link }}" target="_blank">
							  {% if story.author_img %}
									<img class="author-img" src="/assets/img/home/{{ story.author_img }}" alt="{{ story.author }}"/>
								{% endif %}
	              <div class="author-details">
	                <div class="name">{{ story.author }}</div>
	                  <a href="{{ story.position_link }}" target="_blank"><div class="position">{{ story.position }}</div></a>
	              </div>
							</a>
            </div>
          </div>
          <div class="col s12 m6 stories-img">
            <a href="{{ story.screenshot_link }}" target="_blank">
              <img class="responsive-img" src="/assets/img/home/{{ story.screenshot }}" alt="{{ story.short_title }} Screenshot"/>
            </a>
          </div>
        </div>
      </div>
			{% endfor %}
    </div>
  </div>

  <div class="container">
    <div class="section">
			<h2>Wie wir mit Dir arbeiten</h2>
      <!--   Icon Section   -->
      <div class="row">

				{% for proc in site.data.process_de %}
				<div class="col s12 m3">
					<div class="icon-block center">
						<a class="colorless" href="/products">
							<img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
						</a>
						<h4>
              <a class="colorless" href="/products">
                {{ proc.heading }}
              </a>
            </h4>

            <p class="light card-text-align">
              <a class="colorless" href="/products">
				        {{ proc.short_text }}
              </a>
            </p>
					</div>
				</div>
				{% endfor %}
      </div>
    </div>
  </div>

  {% include cta.html %}
