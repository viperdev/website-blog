---
layout: page
permalink: /jobs/
title: Jobs
lang: de
---

Unsere Stellenausschreibungen existieren nur auf Englisch, da wir nur Englische
Bewerbungen akzeptieren.

# Jobs

If you're interested in working at VIPERdev shoot us an email to
[jobs@viperdev.io](mailto:jobs@viperdev.io) with some information about you.

Be sure to phrase the mail in english. You will be judged by an internationally
distributed team.

# Why VIPERdev

- Completely flexible work hours and locations (please be online between 9 to 11am UTC though).
- Flat hierarchy. Need a budget? Change how we work? We embrace change and suggestions and we’ll give you the tools you need to achieve awesome stuff.
- A​ company bike​​ if you want one - less cars, more muscles.
- Height adjustable tables etc. - we understand that body parts are hard to replace.

# Job Openings

<div style="display: flex; flex-basis: content; flex-flow: row wrap; justify-content: center;">
  {% for job in site.data.jobs %}
    <div class="author" style="width: 300px">
      <div class="author-details">
        <div class="name">
          {% if job.file %}
            <a href="/assets/jobs/{{ job.file }}">{{ job.title }}</a>
          {% else %}
            {{ job.title }}
          {% endif %}
        </div>
        <div class="position">{{ job.location }}</div>
        <div class="position">{{ job.type }}</div>
      </div>
    </div>
  {% endfor %}
</div>
