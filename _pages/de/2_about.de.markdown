---
layout: cta-page
title: Über uns
permalink: /about/
lang: de
---

## VIPERdev als Firma

VIPERdev ist ein Startup für Softwareentwicklung aus Hamburg.
Wir sind Entwickler - aber auch erfahrene Gründer.
Deshalb haben die Bedürfnisse unserer Kunden Priorität und nicht einfach nur Buzzwords.

Zusammen finden wir heraus, was unsere Kunden wirklich wollen und brauchen. Dadurch setzen wir Softwareideen schnell und günstig in die Realität um.

## Unsere Grundwerte

{% for value in site.data.values_de %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/values/icon_{{ value.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ value.heading }}</h3>
    <p>{{ value.text }}</p>
  </div>
</div>
{% endfor %}

## Team

Unser Kernteam sitzt in Hamburg, wo VIPERdev 2017 gegründet wurde. Zusätzlich haben wir ein internationales Netzwerk unserer besten Entwickler und Gründer aufgebaut, welche uns bei unseren Projekten regelmäßig unterstützen.

<div style="display: flex; flex-basis: content; flex-flow: row wrap;">
  {% for member in site.data.team %}
      <a href="{{ member.linkedin }}" target="_blank">
        <div class="author" style="width: 300px">
          <img class="author-img" src="/assets/img/team/{{ member.name | downcase }}.jpg" alt="{{ member.name }}">
          <div class="author-details">
            <div class="name">{{ member.name }}</div>
            <div class="position">{{ member.position }}</div>
            <div class="position">{{ member.location }}</div>
            {% if member.mail %}
            <div class="position">
              <a href="mailto:{{ member.mail }}">Mail Me</a>
            </div>
            {% endif %}
          </div>
        </div>
      </a>
  {% endfor %}
</div>
