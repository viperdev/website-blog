---
layout: page
permalink: /terms/
title: Allgemeine Geschäftsbedingungen
lang: de
---

## Geschäftsbedingungen für die Entwicklung einer „Minimum Viable Product“ Software sowie Support- und Wartungsleistungen

### 1. Allgemeine Regelungen


1. Diese Geschäftsbedingungen gelten nur gegenüber Unternehmern im Sinne von § 14 BGB, juristischen Person des öffentlichen Rechts oder öffentlich-rechtlichen Sondervermögen. Sie gelten nicht nur für das Vertragsverhältnis, in das sie einbezogen wurden, sondern auch für alle zukünftigen Geschäftsbeziehungen, wenn der Auftragnehmer auf keine anderen Geschäftsbedingungen verweist.
2. Abweichende, entgegenstehende oder ergänzende Allgemeine Geschäftsbedingungen des Auftraggebers werden, selbst bei Kenntnis des Auftragnehmers, nicht Vertragsbestandteil, es sei denn, ihrer Geltung wird ausdrücklich zugestimmt.
3. Angebote des Auftragnehmers sind freibleibend. Mit der Annahme eines Angebotes oder der Unterbreitung eines eigenen Angebotes erklärt der Auftraggeber verbindlich, die ausgewiesene Leistung beauftragen zu wollen. Der Auftragnehmer ist berechtigt, dass in der Bestellung liegende Vertragsangebot innerhalb einer Woche nach Zugang anzunehmen. Die Annahme kann entweder ausdrücklich oder durch den Beginn der Bearbeitung der Bestellung erklärt werden.

### 2. Grundlagen der Softwareentwicklung


1. Der Auftraggeber möchte vom Auftragnehmer eine Software als sog. Minimum Viable Product entwickeln lassen. Dies ist eine Software, die nur eine erste, minimal funktionsfähige Iteration in der Softwareentwicklung darstellt, anhand deren Kunden-, Markt- oder Funktionsbedarf weiter evaluieren werden können. Die Software wird zu diesem Zweck möglichst schnell entwickelt und nur mit nötigsten Kernfunktionen ausgestattet. 
2. Aufgrund dieses Ansatzes ist die zu liefernde Software keine fertige Entwicklung, sondern nur eine erste Stufe im Prozess der Entwicklung der finalen Software. Im Interesse der Schnelligkeit der Entwicklung beschränkt man die Entwicklung auf bestimmte Funktionen und es erfolgt keine umfassende Prüfung auf Fehlerfreiheit. Im Vordergrund stehen vielmehr Schnelligkeit und Kostengünstigkeit der Entwicklung. 
3. Etwaige Fehler der Leistung, welche aus diesen Prämissen folgen und damit von den Parteien zur Erreichung der Ziele billigend in Kauf genommen werden, stellen demgemäß keine Mängel der Leistung im Rechtssinne dar.
4. Diese Grundsätze sind wesentliche Prämisse der Beauftragung des Auftragnehmers und der von ihm zu erbringenden Leistungen.

### 3. Gegenstand der Beauftragung


1.  Gemäß den vorstehenden Grundlagen der Beauftragung beauftragt der Auftraggeber den Auftragnehmer mit der Erbringung von Programmier- und damit im Zusammenhang stehender Konzeptions- und Beratungsleistungen für die Erstellung der im Angebot grob beschriebenen Software als Minimum Viable Product.
2.  In der ersten Phase wird der Auftragnehmer die Leistungsbeschreibung des Angebotes bis zum dem im Angebot angegebenen Termin fortschreiben und die Minimalanforderungen an die zu erstellende Software definieren. Mit Freigabe dieser Beschreibung durch den Auftraggeber beginnt der Auftragnehmer mit der Erstellung der entsprechenden Software.
3.  Auf Wunsch des Auftragnehmers werden sich die Parteien laufend zu der Entwicklung der Software abstimmen.
4.  Sofern im Angebot ausgewiesen, wird der Auftragnehmer auch mit der Erbringung von Support- und Wartungsleistungen gemäß Ziffer 8 beauftragt.

### 4. Art und Weise der Softwareentwicklung durch den Auftragnehmer


1.  Der Auftragnehmer erbringt seine Leistungen nach dem Stand der Technik.
2.  Der Auftragnehmer kann über die zu verwendende Entwicklungsumgebung sowie über die Benutzung von Bibliotheken, Frameworks etc. frei entscheiden, solange diese für den Auftraggeber kostenfrei und zu üblichen Bedingungen erlangt werden können. Sind diese kostenpflichtig, ist die vorherige Zustimmung des Auftraggebers einzuholen. Der Auftraggeber ist darüber zu informieren, welche Tools für die Erstellung der Software verwendet wurden sowie ggf. für die ablauffähige Kompilierung erforderlich und wo die hierfür erforderlichen Programme, Frameworks etc. zum Download erhältlich sind.
3.  Der Auftragnehmer wird dem Auftraggeber in einem angemessenen Umfang über wesentliche Fortschritte seiner Leistungen informieren und ihm auf dessen Wunsch im angemessenen Umfang den jeweils aktuellen Stand seiner Leistungen vorführen.

### 5. Lieferung der Software


1.  Mit Fertigstellung der Software wird der Auftragnehmer diese dem Auftraggeber zur Abnahme zur Verfügung stellen. Die Fertigstellung soll bis zum dem im Angebot genannten Termin erfolgen, mangels Angabe in angemessener Frist.
2.  Eine Entwicklungsdokumentation, ein Installations- und Administrationshandbuch oder ein Benutzerhandbuch ist vor dem Hintergrund, dass nur eine Minimum Viable Product Software geliefert wird, ohne ausdrückliche Vereinbarung nicht geschuldet. Der Auftraggeber erhält jedoch alle Informationen, die für die Installation und den Betrieb der Software erforderlich sind.
3.  Der Auftraggeber ist berechtigt, die Software für die Zwecke der Abnahme produktiv einzusetzen. Eine stillschweigende Abnahme ist erklärt, wenn der Auftraggeber auf schriftliche Aufforderung des Auftragnehmers, binnen einer Frist von mindestens zwei Wochen nach Zugang der Aufforderung die Abnahme zu erklären oder die Abnahme verhindernde Mängel zu rügen, sich nicht entsprechend erklärt.

### 7. Rechteübertragung für die Software


1.  Der Auftraggeber hat für die Software das übertragbare, nicht exklusive, unbefristete, unwiderrufliche, zeitlich, räumlich und sachlich unbeschränkte Recht, diese zu nutzen, zu verwerten, zu speichern, zu bearbeiten, zu erweitern, zu verbreiten, zu vervielfältigen, öffentlich wiederzugeben, auf andere Datenträger zu übertragen, in Bild und Ton wiederzugeben oder sonst zu verändern.
2.  Ein Hinweis auf den jeweiligen Urheber ist vom Auftraggeber nicht geschuldet.
3.  Die Software darf sog. Open Source Software enthalten. An dieser erwirbt der Auftraggeber Rechte gemäß den für diese geltenden Lizenzbestimmungen. Der Einsatz von Open Source Software hat dabei in der Art und Weise zu erfolgen, dass individuell für den Auftraggeber erbrachte Leistungen nicht selbst unter die jeweilige Open Source Lizenz fallen, es sei denn, der Auftraggeber war hierüber ausdrücklich informiert und hat dem zugestimmt.

### 8. Support und Wartung


1.  Sofern Gegenstand des Auftrages erbringt der Auftragnehmer ergänzend die vereinbarten Support- und Wartungsleistungen für die Software. Für diese Zwecke wird der Auftragnehmer das von ihm angebotene Monitoringtool auf dem entsprechenden Server installieren und die Meldungen des Tools während seiner Bürozeiten überwachen und auswerten.
2.  Sofern das Monitoringtool Fehler der Software oder andere Fehlfunktionen meldet, wird der Auftragnehmer während seiner Arbeitszeiten in angemessener Frist die Arbeiten mit dem Ziel der Behebung aufnehmen. Der Auftragnehmer schuldet lediglich das Bemühen der Behebung, nicht jedoch einen konkreten Erfolg.
3.  Über die Support- und Wartungsleistungen rechnet der Auftragnehmer zu den vereinbarten Sätzen ab. Sofern mit dem Auftraggeber ein monatliches Stundenbudget vereinbart ist, wird der Auftragnehmer ihn informieren, wenn für einen Kalendermonat erkennbar ist, dass dieses überschritten wird. Nicht genutzte Stundenbudgets sind nicht in den Folgemonat übertragbar.
4.  An den im Rahmen des Supports erbrachten Leistungen erwirbt der Auftraggeber Rechte wie an der vertragsgegenständlichen Software.
5.  Sofern der Auftragnehmer im Rahmen dieser Tätigkeit mit personenbezogenen Daten des Auftraggebers so in Berührung kommen kann, dass eine Auftragsverarbeitung nach Art. 28 DSGVO vorliegt, werden die Parteien unverzüglich einen entsprechenden Vertrag abschließen.

### 9. Entgelt


1.  Dem Auftragnehmer steht für seine Tätigkeit das im Auftrag vereinbarte Entgelt zu. Eine höhere als die vereinbarte Vergütung steht ihm nur dann zu, wenn die Erhöhung ausdrücklich und unter Angabe eines konkreten Betrages mit dem Auftraggeber vereinbart ist.
2.  Eine Pauschalvergütung ist zu jeweils 1/3 mit Beauftragung, Lieferung der abnahmebereiten Software und mit Abnahme zur Zahlung fällig, sofern nicht anders vereinbart.
3.  Über eine Vergütung nach Aufwand rechnet der Auftragnehmer jeweils monatlich nachträglich ab. Auf Zeitabschreibung basierenden Rechnungen ist für deren Fälligkeit eine aussagekräftige und nachvollziehbare Zeitanschreibung beizufügen, die es dem Auftraggeber ermöglicht, die jeweilige Rechnung auf ihre Richtigkeit zu überprüfen.

### 10. Geheimhaltung


1.  Der Auftragnehmer verpflichtet sich zur Geheimhaltung der Geschäftsgeheimnisse des Auftraggebers. Geschäftsgeheimnisse sind Informationen oder Kenntnisse gleich welcher Art, die im Rahmen dieses Vertrages dem Auftragnehmer bekannt gemacht werden, ihm in diesem Rahmen auf sonstige Art und Weise zur Kenntnis kommen oder vor Ablauf des Vertrages im Rahmen seiner Verhandlung zur Kenntnis gekommen sind, sofern und soweit diese aus der Sphäre des Auftraggebers stammen. Informationen und Kenntnisse können in beliebiger Form zur Kenntnis gebracht werden (schriftlich, mündlich, per Email etc.). Es ist nicht erforderlich, dass ein Geschäftsgeheimnis als solches bezeichnet wird
2.  Nicht der Geheimhaltung unterliegen Geschäftsgeheimnisse, welche (i) zum Zeitpunkt der Übermittlung allgemein bekannt waren oder danach - ohne Verschulden des Auftragnehmers - allgemein bekannt werden, (ii) seitens des Auftragnehmers bereits zum Zeitpunkt der Offenbarung ohne Bestehen einer Geheimhaltungsverpflichtung rechtmäßig bekannt waren, (iii) nach dem Zeitpunkt der Übermittlung von Seiten Dritter dem Auftragnehmer rechtmäßig ohne Geheimhaltungsverpflichtung bekannt gemacht werden, ohne dass die dritte Seite ihrerseits zur Geheimhaltung gegenüber dem Auftraggeber verpflichtet ist oder (iv) aufgrund zwingender gesetzlicher, behördlicher oder gerichtlicher Vorschriften bzw. Anordnungen offenbart werden müssen.
3.  Der Auftragnehmer ist verpflichtet, Geschäftsgeheimnisse des Auftraggebers geheim zu halten und keinem Dritten Kenntnis von diesen zu ermöglichen. Eine Weitergabe der Geschäftsgeheimnisse an Dritte ist nur mit Zustimmung des Auftraggebers möglich, der diese nicht unbillig verweigern darf. Die Dritten sind entsprechend dieses Vertrages durch den Empfänger zur Geheimhaltung zu verpflichten. Geschäftsgeheimnisse des Auftraggebers sind durch den Auftragnehmer nur im zwingend erforderlichen Umfang zu vervielfältigen. Es ist dem Auftragnehmer untersagt, dieses zu einem anderen, als den vertraglichen Zweck zu nutzen oder nutzen zu lassen.
4.  Der Auftraggeber kann vom Auftragnehmer jederzeit die Rückgabe der ihm in körperlicher Form überlassenen Geschäftsgeheimnisse verlangen. Diese Verpflichtung besteht für den Auftragnehmer auch für sämtliche körperlichen Kopien oder sonstigen körperlichen Vervielfältigungen, die er von Geschäftsgeheimnissen des Auftraggebers erstellt hat. Dem Auftragnehmer in digitaler Form überlassene Geschäftsgeheimnisse sind auf Wunsch des Auftraggebers vollständig in einer Art und Weise zu löschen, dass ihre Wiederherstellung ausgeschlossen ist. Gesetzliche Aufbewahrungsverpflichtungen bleiben unberührt. Zurückbehaltungsrechte sind ausgeschlossen, es sei denn sie beruhen auf vollstreckbaren Entscheidungen oder auf unbestrittenen Ansprüchen.
5.  Diese Geheimhaltungsvereinbarung wird von einer Kündigung des Vertrages nicht berührt.

### 11. Haftung


1.  Die Haftung richtet sich nach den gesetzlichen Bestimmungen, sofern nicht nachfolgend etwas Abweichendes vereinbart wird.
2.  Bei einfach fahrlässiger Verletzung wesentlicher Vertragspflichten ist die Haftung der Höhe nach beschränkt auf die vorhersehbaren und vertragstypischen Schäden. Wesentliche Vertragspflichten sind solche, deren Erfüllung die ordnungsgemäße Durchführung des Vertrages überhaupt erst ermöglicht und auf deren Einhaltung die verletzte Partei regelmäßig vertrauen darf. Die Verjährungsfrist für Ansprüche nach diesem Absatz beträgt ein Jahr.
3.  Absatz 2 gilt nicht für Ansprüche aus der Verletzung des Körpers, der Gesundheit oder des Lebens, bei arglistigem Handeln, bei Übernahme einer Garantie sowie für Ansprüche nach dem Produkthaftungsgesetz.

### 12. Mängelansprüche


1.  Dem Auftraggeber stehen bei Mängeln der Leistung des Auftragnehmers die gesetzlichen Rechte zu, wobei der Auftragnehmer entscheidet, ob er durch Nachbesserung oder Neulieferung den Mangel behebt.
2.  Für Mängel ist eine Verjährungsfrist von einem Jahr vereinbart. Dies gilt nicht bei der Verletzung von Leib, Leben oder Gesundheit sowie wenn dem Auftragnehmer arglistiges Handeln oder die Übernahme einer Garantie vorzuwerfen ist.

### 13. Schlussbestimmungen


1.  Dieser Vertrag enthält alle Vereinbarungen der Parteien zum Vertragsgegenstand. Etwaig abweichende Nebenabreden und frühere Vereinbarungen zum Vertragsgegenstand werden hiermit unwirksam.
2.  Änderungen und Ergänzungen dieses Vertrages bedürfen der Schriftform, soweit nicht gesetzlich eine strenge Form vorgeschrieben ist. Dies gilt auch für jeden Verzicht auf das Formerfordernis.
3.  Allgemeine Geschäftsbedingungen der Parteien finden auf diesen Vertrag keine Anwendung. Dies gilt auch dann, wenn auf deren Einbeziehung in späteren Dokumenten, die im Zusammenhang mit diesem Vertrag stehen (z.B. Abruf von Leistungen) unwidersprochen hingewiesen wurde.
4.  Sollte eine Bestimmung dieses Vertrages ganz oder teilweise nichtig, unwirksam oder nicht durchsetzbar sein oder werden, oder sollte eine an sich notwendige Regelung nicht enthalten sein, werden die Wirksamkeit und die Durchsetzbarkeit aller übrigen Bestimmungen dieses Vertrages nicht berührt. Anstelle der nichtigen, unwirksamen oder nicht durchsetzbaren Bestimmung oder zur Ausfüllung der Regelungslücke werden die Parteien eine rechtlich zulässige Regelung vereinbaren, die so weit wie möglich dem entspricht, was die Parteien gewollt haben oder nach dem Sinn und Zweck dieses Vertrages vereinbart haben würden, wenn sie die Unwirksamkeit oder die Regelungslücke erkannt hätten. Beruht die Nichtigkeit einer Bestimmung auf einem darin festgelegten Maß der Leistung oder der Zeit (Frist oder Termin), so gilt die Bestimmung mit einem dem ursprünglichen Maß am nächsten kommenden rechtlich zulässigen Maß als vereinbart. Es ist der ausdrückliche Wille der Parteien, dass diese salvatorische Klausel keine bloße Beweislastumkehr zur Folge hat, sondern § 139 BGB insgesamt abbedungen ist.
5.  Alleiniger Gerichtsstand für alle Streitigkeiten im Zusammenhang mit dieser Vereinbarung ist der Sitz des Auftragnehmers.
