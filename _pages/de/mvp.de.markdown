---
layout: page
permalink: /minimum-viable-product/
title: MVP
lang: de
---

## What is an MVP?

Ein [MVP (Minimum Viable Product)](https://www.techopedia.com/definition/27809/minimum-viable-product-mvp) ist die minimale Lösung, um Deine Geschäftsannahme zu testen oder Deinen Benutzer zufrieden zu stellen.

Für jedes Feature, das Du gedenkst zu gebrauchen, frage Dich einfach: *ist diese Funktion absolut notwendig, um das Problem für meine Benutzer zu lösen*?

Wenn die Antwort nein sein sollte, dann vernachlässige es.

Unsere MVP-Philosophie ermöglicht es uns, Apps für unsere Kunden mit minimalen Investitionen zu erstellen. Wir kümmern uns um Dein Geschäft und nicht nur über Funktionen.

Wenn Du mehr über MVPs lesen möchtest, lese einfach [diesen Artikel](https://medium.com/swlh/how-to-build-an-mvp-that-does-its-job-if-youre-a-non-tech-founder-3a0d5d01858f){:target="_blank"}.
