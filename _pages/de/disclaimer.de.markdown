---
layout: page
permalink: /disclaimer/
title: Haftungsausschluss
lang: de
---

## Haftungsausschluss

### Verantwortlichkeit für den Inhalt

Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Nach den gesetzlichen Bestimmungen sind wir weiterhin für eigene Inhalte auf diesen Webseiten verantwortlich. In diesem Zusammenhang weisen wir darauf hin, dass wir demzufolge nicht verpflichtet sind, die lediglich übermittelten oder gespeicherten fremden Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon gemäß §§ 8 bis 10 des Telemediengesetzes (TMG) unberührt.

### Verantwortlichkeit für Links

Für den Inhalt externer Links (auf Webseiten Dritter) sind ausschließlich deren Betreiber verantwortlich. Zum Zeitpunkt der Verlinkung waren uns keine Rechtsverstöße ersichtlich. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.

### Urheberrechte

Unsere Webseiten und deren Inhalte unterliegen dem deutschen Urheberrecht. Sofern nicht ausdrücklich gesetzlich gestattet (§§ 44a ff. Urhebergesetz), bedarf jede Form der Verwertung, Vervielfältigung oder Bearbeitung urheberrechtlich geschützter Werke auf unseren Webseiten der vorherigen Zustimmung des jeweiligen Rechteinhabers. Individuelle Reproduktionen eines Werkes sind nur für den privaten Gebrauch gestattet, dürfen also weder direkt noch indirekt zum Verdienst dienen. Die unerlaubte Verwendung urheberrechtlich geschützter Werke ist strafbar (§ 106 Urheberrechtsgesetz).

### Cookies

Diese Webseite benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Besucher ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird Ihre IP-Adresse nicht mit anderen Daten von Google in Verbindung bringen. Sie können die Verwendung von Cookies ablehnen, indem Sie die entsprechenden Einstellungen in Ihrem Browser auswählen. Bitte beachten Sie jedoch, dass Sie unter Umständen nicht den vollen Funktionsumfang dieser Website nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Verarbeitung Ihrer Daten durch Google in der zuvor beschriebenen Art und Weise und zu den zuvor genannten Zwecken einverstanden. Sie können die Erfassung und Verwendung von Daten (Cookies und IP-Adresse) durch Google verhindern, indem Sie das Browser-Plugin intallieren. Bitte beachten Sie, dass diese Webseite Google Analytics mit der Einstellung "anonymizeIp" beinhaltet. Dies garantiert eine anonymisierte Datenerhebung durch Maskierung des letzten Teils Ihrer IP-Adresse. Weitere Informationen zu den Nutzungsbedingungen und zum Datenschutz finden Sie in den Google Analytics-Nutzungsbedingungen oder unter Datenschutzübersicht

### Bildnachweise

Vektor-Grafiken: freepik.com
