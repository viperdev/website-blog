---
layout: page
permalink: /imprint/
title: Impressum
lang: de
---

## Impressum

Informationen nach Abschnitt 5 Telemediengesetz (TMG):

Viper Development UG (haftungsbeschränkt)  
Harburger Schlossstrasse 6-12  
21079 Hamburg, Germany  
HRB 147102  
Telefon: 040 766293720  
E-Mail: info@viperdev.io  
Website: www.viperdev.io  
Umsatzsteuer-Identifikationsnummer gem. § 27 a Umsatzsteuergesetz: DE312932940  
Verantwortliche i.S.d. § 55 Abs. 2 RStV: Lasse Schuirmann, Harburger Schlossstraße 6-12, 21079 Hamburg  
