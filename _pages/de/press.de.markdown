---
layout: page
permalink: /press/
title: Presse
lang: de
---


# VIPERdev Presseinformationen

Auf dieser Seite erfährst Du, wie Du unser Logo verwendest und / oder in der Öffentlichkeit über VIPERdev.io schreiben kannst.

## Textbenutzung

Verwende den Namen VIPERdev immer im passenden Kontext.

Wenn Du VIPERdev ohne Kontext verwendest, benutze "VIPERdev.io", um den Leuten das Verständnis zu erleichtern, zu erfahren, worum es geht.

> Übrigens - ich habe gerade VIPERdev.io gefunden - sie können tatsächlich
> funktionierende Apps in wenigen Wochen erstellen!

Wenn VIPERdev in längeren Texten mit dem richtigen Kontext referenziert wird, wirkt das ".io"
schwerfällig und sollte weggelassen werden. Vor allem, wenn es kontextuell klar ist, was Du tust, ist die explizite Referenz nicht notwendig.

> Das VIPERdev-Team verbessert ständig ihre wiederverwendbaren Komponenten um
> Kosten und Zeitaufwand für alle Kunden gering zu halten - so werden Deine Apps jeden Monat besser
> und bestehende Kunden können aktualisierte Komponenten erhalten, ohne in mehr Entwicklungsstunden investieren zu müssen.

## Logoverwendung

Bitte verwende unser Logo auf einem hellen Hintergrund, benutze unser [PNG](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev.png){:target="_blank"}.

Sie können unser einfaches Logo (ohne Text) [hier](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev_without_font.png){:target="_blank"} herunterladen.

Für Vektorgrafiken lade einfach unsere [AI-Datei](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev%20logo%20final.ai){:target="_blank"}.
