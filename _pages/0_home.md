---
layout: default
title: Home
permalink: /
lang: en
---

<div id="titlecover" >
  <div class="title-carousel">
    <div class="carousel-item" style="background-image:url(../assets/img/home/ladder.jpg)"></div>
  </div>
	<div class="title-slogan">
		<div>
			<h1><span>Your Software Product. Done in 8&nbsp;Weeks.</span></h1>
		</div>
		<div>
			<h2><span>Don’t get super-sized! We develop software you really need. </span></h2>
		</div>
	</div>
  <div class="title-bottom">
  	<div class="social-links" id="social-media-links">
  		<ul>
  			<li><a href="https://twitter.com/viperdev_io" target="_blank" class="twitter"></a></li>
  			<li><a href="https://www.linkedin.com/company/viperdev/" target="_blank" class="linkedin"></a></li>
  			<li><a href="/startproject" target="_blank" class="envelope"></a></li>
  			<li><a href="/feed.xml" target="_blank" class="rss" title="zum Newsletter anmelden"></a></li>
  		</ul>
  	</div>
  	<a href="#site-header" id="scroll-to-content"></a>
	</div>
</div>

<div class="container section center">
  <h3>
    Get your idea to the market in just 8 weeks.&nbsp;&nbsp;
    <a href="/en/products/" class="btn-large">OUR PRODUCTS</a>
  </h3>
</div>

<div class="stripe z-depth-1">
  <div class="container section row">
    <div class="col s12 m3">
      <a class="colorless" href="/about">
        <img class="viperdev_img center" src="/assets/img/viperdev_footer.png" alt="VIPERdev Logo">
      </a>
    </div>
    <div class="col s12 m9">
      <p style="text-align:justify">
        <a class="colorless" href="/about">
        VIPERdev is a software development startup from Hamburg, Germany. We are developers. But we are also experienced founders. That’s why we always put our clients needs first – not chargeability or expenses.
        Together we find out what you really want. Being on the same page we implement your software ideas into actual products fast and affordably.
        </a>
      </p>
    </div>
  </div>
</div>


<div class="section container">
	<h3>
		Success Stories
	</h3>
	<div class="carousel-class">
		{% for story in site.data.success_en %}
		<div data-title="{{ story.short_title }}">
			<div class="row">
				<div class="col s12 m6">
					<h4>{{ story.title }}</h4>
					<blockquote><p>{{ story.text }}</p></blockquote>
					<div class="author">
						<a href="{{ story.author_link }}" target="_blank">
							{% if story.author_img %}
								<img class="author-img" src="/assets/img/home/{{ story.author_img }}" alt="{{ story.author }}"/>
							{% endif %}
							<div class="author-details">
								<div class="name">{{ story.author }}</div>
									<a href="{{ story.position_link }}" target="_blank"><div class="position">{{ story.position }}</div></a>
							</div>
						</a>
					</div>
				</div>
				<div class="col s12 m6 stories-img">
					<a href="{{ story.screenshot_link }}" target="_blank">
						<img class="responsive-img" src="/assets/img/home/{{ story.screenshot }}" alt="{{ story.short_title }} Screenshot"/>
					</a>
				</div>
			</div>
		</div>
		{% endfor %}
	</div>
</div>

<div class="container">
  <div class="section">
		<h2>How we work with you</h2>
    <!--   Icon Section   -->
    <div class="row">

			{% for proc in site.data.process_en %}
			<div class="col s12 m3">
				<div class="icon-block center">
					<a class="colorless" href="/products">
						<img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
					</a>
					<h4>
            <a class="colorless" href="/products">
              {{ proc.heading }}
            </a>
          </h4>

          <p class="light card-text-align">
            <a class="colorless" href="/products">
			        {{ proc.short_text }}
            </a>
          </p>
				</div>
			</div>
			{% endfor %}
    </div>
  </div>
</div>

{% include cta.html %}
