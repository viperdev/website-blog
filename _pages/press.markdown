---
layout: page
permalink: /press/
title: Press    
lang: en
---


# VIPERdev Press Kit

This page explains how you can use our logo and/or write about VIPERdev.io in public.

## Textual Usage

Always spell VIPERdev with the proper casing.

When using VIPERdev in without context, use "VIPERdev.io" to make it easy for
people to understand what this is about.

> By the way - I just found VIPERdev.io - they can make actual working apps in
> mere weeks!

When referencing VIPERdev in longer texts with proper context, the ".io" is
cumbersome and to be omitted. Especially if it is contextually clear what you
refer to, the explicit reference is not needed.

> The VIPERdev team is constantly improving their reusable components to keep
> costs and time efforts down for all clients - this way their apps get better
> every month and existing clients can receive updated components without
> having to invest in may more development hours.

## Logo Usage

Please use our logo on a light background, use our [PNG](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev.png){:target="_blank"}.

You can download our plain logo (without text) [here](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev_without_font.png){:target="_blank"}.

For vector graphics, simply download [our AI file](https://gitlab.com/viperdev/open/artwork/blob/master/logo/viperdev%20logo%20final.ai){:target="_blank"}.
