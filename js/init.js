var cookiePopup;
(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.carousel').carousel();
    $(document).ready(function(){
      $('.carousel-class').slick({
        arrows: true,
        dots:true,
        customPaging : function(slider, i) {
          var title = $(slider.$slides[i]).data('title');
          return '<a class="pager__item"> '+title+' </a>';
        }
      }); 
      
      //initialize cookie consent
      cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "#edeff5",
            "text": "#838391"
          },
          "button": {
            "background": "#2657bf"
          }
        },
        "content": {
          "href": "/disclaimer"
        },
        onInitialise: function (status) {
          var type = this.options.type;
          var didConsent = this.hasConsented();
          if (type == 'opt-in' && didConsent) {
            // enable cookies
          }
          if (type == 'opt-out' && !didConsent) {
            window['ga-disable-UA-92978878-4'] = true;
          }
        },

        onStatusChange: function(status, chosenBefore) {
          var type = this.options.type;
          var didConsent = this.hasConsented();
          if (type == 'opt-in' && didConsent) {
            window['ga-disable-UA-92978878-4'] = false;
          }
          if (type == 'opt-out' && !didConsent) {
            window['ga-disable-UA-92978878-4'] = true;
          }
        },

        onRevokeChoice: function() {
          var type = this.options.type;
          if (type == 'opt-in') {
            //disable cookies
          }
          if (type == 'opt-out') {
            // enable cookies
          }
        }
      }, function(popup){
        cookiePopup = popup;
      });
		$('.title-carousel').slick({
			autoplay: true,
			autoplaySpeed:10000,
			speed:1000,
			arrows: false,
			fade: true
		});
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space
