---
layout: post
title:  "VIPERdev at Disrupt Berlin 2018"
date:   2018-11-27 11:55 +0100
author: "Lolita Ljametova"
profile_link: ""
categories: Events
lang: en
image:
  feature: hands-with-puzzles.png
  credit: "Freepik.com"
---

On 29th and 30th November the [„Disrupt Berlin 2018“](https://techcrunch.com/events/disrupt-berlin-2018/#a28769d5-3890-46e1-a2b3-a487a3af295e) opens it's doors again. The online publisher of technology industry news „TechCrunch“ invites all kinds of startups and companies to exchange their technological experiences and news during the exhibition. The event is going to be at the „Arena Berlin“.
TechCrunch is calling everyone a startupper – no matter if you‘re a founder, investor or hacker – everyone‘s welcome.
The Agenda is full of interesting presentations by founders and famous personalities (e.g. Lucas Di Grassi) and even a pitch between 15 tech-startups, the winner of the competition will get a prize of $50.000.

And of course we VIPERdev is going to be there too. On day 2 we‘re going to be at the Startup Alley. As a startup, known for software development, we are interested to Network with the brightest innovators and companies in the world. And we would like to show the importance of MVP‘s.

Asking yourself what‘s a MVP? - it‘s pretty easy:

MVP or minimum viable product is a development technique with just enough features to satisfy early customers as fast as possible to define the target market. The keywords are EASY and FAST. VIPERdev creates the best technological solution within 30 days in relation to the needs of our customers.

Still asking yourself how this works or how to start such a project? Just contact us at the
„Disrupt Berlin 2018“ we‘ll be waiting for you.
