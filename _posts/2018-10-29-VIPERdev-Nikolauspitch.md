---
layout: post
title:  "VIPERdev Helps Social Startups with Custom Software Worth 10k Euro"
date:   2018-10-29 17:48 +0100
author: "James Hiller"
profile_link: "https://www.linkedin.com/in/james-hiller-324734160"
categories: SME's
lang: en
image:
  feature: VIPERdev_Nikolauspitch.png
  credit: ""
---

In most of our western civilization, startups are hard enough to get public funding. Or even gain a seed financing from an investor. For social startups it is even a little bit harder. Not only a few use grandma's savings stocking or their own savings for an honorable goal: to make the world a little bit better! For this reason we invite all interested social startups to apply for the [NikolausPITCH](https://nikolauspitch.de/).

You can win prizes between 700 and 10,000 Euros. The winner will receive a free mobile or webapp for his cause to improve the world. The award ceremony will take place on December 6th (St. Nicholas Day!) from 17 to 19 clock in the Startup Dock, Harburger Schlossstrasse 6-12, 21079 Hamburg-Harburg. The deadline for application is 30th November. To participate as a social startup or curious attendee please visit our website: <a href="https://NikolausPITCH.de">https://nikolauspitch.de</a> visit.

We thank Hamburg News for the multimedia coverage of our contest and hope for more news from the Hamburg media and startup scene.

<a href="https://www.hamburg-news.hamburg/de/medien-it-kreativwirtschaft/nikolaus-pitch-foerderung-von-sozialen-startups" target="_blank">https://www.hamburg-news.hamburg/de/medien-it-kreativwirtschaft/nikolaus-pitch-foerderung-von-sozialen-startups</a>
