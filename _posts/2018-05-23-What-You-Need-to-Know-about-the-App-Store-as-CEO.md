---
layout: post
title:  "What You Need to Know about the App Store as CEO"
date:   2018-05-23 2:52:21 -0500
author: "Whitney Tennant"
profile_link: "https://www.linkedin.com/in/whitney-tennant-11a691151/"
categories: Startup
lang: en
image:
  feature: brain.png
  credit: "Freepik.com"
---

As the owner of a business you need to be part of the process of developing your app. It’s imperative to understand the fees and how to set up your business’s account with the App Store. 

Though this may feel like a reluctant distraction from running your business - knowing a bit about app development is not only a necessity but a key skill for business these days.
 
While assisting many startups launch apps with [VIPERdev](https://viperdev.io/), I’ve noticed how bizarre it feels to inform our clients that they need to set up developer accounts with Google, Apple or Windows. 

Setting up a developer account seems like something [we](https://viperdev.io/) as app developers and consultants should be able to set up. However, there are very specific reasons your app developer cannot create this account for you. This post explains what you need to do and pay as the business owner in order to publish your app. 
 
### Creating a platform specific developer account

All of the App Stores (Google, Apple and Windows) require a developer account in order to publish your app. This will allow you access to the respective platform’s developer portal where you set payment options, view your app’s comments, ratings and much more.  Your app is linked to this developer account and is created by you, the business owner, and not by your app developer. 

Listed below are the respective fees for each platform with links for more detailed information of the documentation required. Creating this account will mean sharing private documentation and payment information, thus it must be done by you - the business owner. 

Once created you will need to setup an admin user for your app developer to complete the uploading and signing process. Your app developer will need a platform specific developer account for each of the platforms you intend to launch you app with.

**Google**
Create your developer account [here](https://play.google.com/apps/publish/signup/). You will need to pay a once off $25 fee. It can take up to 48 hours to be processed and registered completely.

**Apple**
Create an Apple developer account [here](https://developer.apple.com/programs/enroll/). You will be able to see the requirements for enrolling as an organisation there too. It will cost $99 per year to have this account.

**Windows**
Enroll for your developer account [here](https://developer.microsoft.com/en-us/store/register). It will cost approximately $99 once-off for businesses.  

### Create admin accounts for your developer

Google, Apple and Windows developer accounts will also allow you to add various people into different roles. Since you will create the developer account - you will be the account owner, it is then possible to add admin users who have sufficient permission to perform important tasks. You should create an admin user on each platform for your app developer so that they can sign and upload your app for you. 

### Signing and uploading your application

Signing is a process that all app stores require and is used to prove that the developer account associated with your company produced the application. Via your developer console for each platform you will be able to generate a certificate or keystore which a developer will use to ‘sign’ your application code. Your app developer will be able to do this for you once they have been added as an admin user on the relevant platform’s developer portal. After signing the app they will be able to upload it. After uploading the app, it is reviewed. Review time is usually 48 hours at maximum. 

### Launch timing 
![Alarm Clock](/assets/img/blog/alarm_clock.png){:.img-feature}
Created by: Freepik.com

Make sure you have set up these accounts while your app is getting developed. It can take a while to gather the required documentation and get evaluated after submitting your documentation. Keep in mind that before you launch your app the code will have to be reviewed by Google, Apple or Windows. This can take 48 hours or longer. You may need to amend some features and resubmit.


You will also need to be certain about how you intend to monetize your application as this is set during the upload and launch phase. We will be writing about monetizing your app soon. Be sure to regularly check our [blog](https://viperdev.io/blog/) for more insightful articles on startups and app development. 
