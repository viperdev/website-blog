---
layout: post
title:  "VIPERdev Takes 3rd at StartUp Slam"
date:   2018-08-29 2:00:00 -0500
author: "James Hiller"
profile_link: "https://www.linkedin.com/in/james-hiller-324734160"
categories: Startup
lang: en
image:
  feature: StartupSlamPic.png
  credit: "12min.me"
---

It was a successful event as always: Already a few days before the event, it was fully booked on meetup. There were 350 registrations via the platform for the startup Slam # 8; the audience was fantastic and provided a breathtaking setting. 12min.me, this is a regular format in various major German cities, giving the presenter the opportunity to introduce their ideas or, in this case, their business model within 12 minutes. Time is stopped always.
![StartUpSlamLasse](/assets/img/blog/StartupSlamLasse.jpg){:.img-feature}
*Group picture with all participants at the end of the Startup Slam event*      
Created by: 12min.me

Every interested startup is advised to take part in such an event and to face the jury. Even if you do not reach the first place, feedback is very helpful in most cases.
