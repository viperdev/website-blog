---
layout: post
title:  "NikolausPITCH - Und die Gewinner sind..."
date: 2019-02-05 9:48 +0100
author: "Lolita Ljametova"
profile_link: "https://www.linkedin.com/in/lolita-ljametova-756238114/"
categories: Events
lang: de
image:
  feature: Nikolaus_Pitch_HEI_3512_Anne_Gaertner.jpg
  credit: "Anne Gaertner Fotografie"  
---

Am Nikolausabend konnte unser Team von VIPERdev Kandidaten mit sozialem Unternehmertum einladen ihre App-Idee vorzustellen, um die Welt ein kleines bisschen besser zu machen.
Grundgedanke war es, sozialen Gründern/-innen mittels einer digitalen Lösung zu helfen einen gesellschaftlichen Nutzen zu schaffen.
Somit standen bei unserem Event sechs Finalisten vor einer unabhängigen Jury.

![Tobias Kästele](/assets/img/blog/Nikolaus_Pitch_HEI_3292_Anne_Gaertner.jpg){:.img-feature}
Erstellt von: Anne Gaertner Fotografie

Wir durften zu unserem Event Tobias Kästele und Christian Wiebe von “Viva con Agua“ begrüßen, die uns zwei voneinander unabhängige soziale Projekte vorstellten. Wie die meisten wissen setzt sich der gemeinnützige Verein “Viva con Agua de Sankt Pauli e.V.“ dafür ein, dass alle Menschen weltweit zugriff auf sauberes Trinkwasser haben.

Auch Kevin Fuchs stellte uns beim Pitch sein soziales Projekt vor, es handelt sich um “Gexsi“, eine Suchmaschine dessen durch Suchanfragen generierten Einnahmen in soziale Projekte gesteckt werden.

![Kevin Fuchs](/assets/img/blog/Nikolaus_Pitch_HEI_3290_Anne_Gaertner.jpg){:.img-feature}
Erstellt von: Anne Gaertner Fotografie

Besonders überzeugt war die Jury von Christina Oskui, eine Kinderbuchautorin die nicht nur ihre Werke in Brailleschrift übersetzen lies, sondern sich intensiv mit der Thematik der Sehbehinderung befasst hat und dabei auf ein Problem gestoßen ist, welches sowohl blinde als auch demenzkranke Menschen beschäftigt. Christina Oskui möchte mittels einer Softwarelösung unter anderem Fotos mit Audiodateien bespielen um die Erinnerungen zu speichern und für Menschen mit Einschränkungen oder auch späteren Generationen zugänglich machen zu können. Mit dieser einfallsreichen Idee belegte sie den dritten Platz und gewann eine Strategieberatung, sowie Softwarearchitekturplanung im Wert von 700,-€.

![Christina Oskui](/assets/img/blog/Nikolaus_Pitch_HEI_3455_Anne_Gaertner.jpg){:.img-feature}
Erstellt von: Anne Gaertner Fotografie

Den zweiten Platz sicherte sich Enno Bassen mit seinem interessanten Startup und gleichzeitig auch Netzwerk “curilab“, bei dem es darum geht das Protokollieren in der Pflege mit Hilfe von Technologien zu vereinfachen, sodass mehr Zeit für zwischenmenschliche Beziehungen bleibt. Protokolle werden mittels einer Cloud an alle Nutzer mit Zugriffsrecht zur Verfügung gestellt, so gehen keine Informationen verloren und alles kann in Echtzeit bearbeitet werden. Der sprachliche Dialog soll erfasst werden und mit Hilfe von künstlicher Intelligenz sollen wichtige Informationen und Aufgaben gefiltert und dokumentiert werden.
Für sein soziales Projekt konnte Enno den 2. Preis, in Form einer Strategieberatung, Software-Architekturplanung und App-Design im Wert von 1.000,-€, gewinnen.

![Enno Bassen](/assets/img/blog/Nikolaus_Pitch_HEI_3513_Anne_Gaertner.jpg){:.img-feature}
Erstellt von: Anne Gaertner Fotografie

Der erste Platz ging an Nadine Herbrich und Alessandro Cocco von “ReCycleHero”. “ReCycleHero” ist ein Abhol-Service für Altglas, Altpapier und Pfandflaschen. Das Altglas wird direkt vom Verbraucher abgeholt und für ihn ordnungsgemäß entsorgt. Die Abholung erfolgt per Lastenrad, was die Umwelt zusätzlich schont. Das Team von “ReCycleHero” beschäftigt überwiegend Geflüchtete, Langzeitarbeitslose und Obdachlose, somit wird finanziell Benachteiligten bei der Integration in Job und Gesellschaft geholfen.
So viel soziales Engagement und Umweltbewusstsein wurde von unserer Jury mit einer Softwarelösung im Wert von von 10.000,-€ belohn

![Alessandro Cocco](/assets/img/blog/Nikolaus_Pitch_HEI_3500_Anne_Gaertner.jpg){:.img-feature}
Erstellt von: Anne Gaertner Fotografie

Wir gratulieren den Gewinnern und möchten uns an dieser Stelle bei unserer unabhängigen Jury bestehend aus Katharina Osbelt, Dorothee Vogt und Christian Salzmann bedanken. Ein weiteres Dankeschön geht zudem an unseren Eventpartner, dem StartupDock. 

Wir freuen uns schon auf den nächsten NikolausPITCH und hoffen auf neue soziale Projekte.

