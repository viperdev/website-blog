---
layout: post
title:  Der richtige Weg Features für ein MVP zu definieren
date:   2018-04-25 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: de
image:
  feature: start_up_dock.png
  credit: "Freepik.com"
---

Unternehmer geraten oft in die Falle, zu viel Zeit und Geld in nicht wesentliche Merkmale ihres Produkts zu investieren. Wenn das Startup einfach die wertvollste Idee validieren muss, wie kann man dann Funktionen für ein "Minimum Viable Product" definieren?

### Starte mit dem ‘Warum?’
Die meisten Unternehmen denken über ihre Produkte nur in Bezug auf Funktionen und Statistiken nach. Simon Sinek argumentiert, dass die erfolgreichsten Marken zuerst erklären, warum ihr Produkt wichtig ist. Frage Dich, warum Du das Produkt herstellen möchtest und warum sich die Menschen so viel Mühe geben sollten, es zu kaufen. Dies ist ein guter Ausgangspunkt, um die Funktionen zu definieren, die Du für Dein MVP benötigst. Es gibt Dir die Möglichkeit zu erkennen, **was Dein Produkt von anderen unterscheidet** und auf welche Funktionen man sich zunächst konzentrieren sollte, um diese Qualitäten hervorzuheben.

Die Mitbegründer von Dropbox zum Beispiel glaubten, dass die Dateisynchronisierung ein Problem sei. Davon wussten die meisten Leute jedoch nichts. Die Dropbox-Gründer nahmen an, dass die Menschen, wenn sie einmal die innovative Lösung erfahren hätten, sich nicht vorstellen könnten, wie sie jemals ohne sie gelebt hätten. **Um das Risiko zu vermeiden, Jahre an harter Arbeit und Geld zu investieren**, nur um ein Produkt zu schaffen, das niemand will, machte Drew Houston, Mitbegründer von Dropbox, ein dreiminütiges Video, das demonstrierte, wie die Technologie funktioniert. Diese einfache Idee bestätigte die Annahme, dass Kunden das Produkt haben wollten. Der Clip trieb hunderttausende von Menschen auf ihre Webseite. Dies zeigt perfekt, dass der Beginn mit der Frage "Warum?" im Gegensatz zum Nachdenken über ausufernde Features, zur Validierung Deiner Idee führen kann.

### Priorisiere Deine Funktionen
Das Identifizieren der Kernfunktionen kann eine herausfordernde Aufgabe sein. Gehe die Priorisierung als Teamaktivität an, um unterschiedliche Perspektiven zu erhalten. Um den Prozess zu vereinfachen, kannst Du mit der **MoSCoW-Methode** beginnen, die ein Akronym ist, das aus dem ersten Buchstaben jeder der (englischen) Priorisierungskategorien abgeleitet wird: Muss haben, sollte haben, könnte haben und wird nicht haben. Wenn Du ein innovatives Produkt entwickelst, reichen die "Muss"-Features aus, um zu zeigen, dass es das von Dir identifizierte Kernproblem löst. Nehmen wir als Beispiel eine Rechnungs-App:

* **Muss haben** - Denke an die *muss haben*-Funktionen als Rückgrat Deines MVP. Für eine Rechnungs-App besteht diese Funktion darin, Rechnungen zu erstellen und speichern zu können.
* **Sollte haben** - Diese Funktionen sind hilfreich, aber nicht unbedingt notwendig für die Iteration in der frühen Phase. Die Möglichkeit, an Rechnungen in der Cloud zu arbeiten oder die Rechnung per E-Mail an die Kunden zu senden, sind einige Beispiele für Funktionen, die eine solche Applikation haben sollte.
* **Könnte haben** - Die Applikation *könnte* perfekt gestaltete Funktionen und zusätzliche Features haben, aber es ist am besten, sie in die Schublade zu legen, bis Deine Idee bestätigt wird. Zum Beispiel: Integration der App mit anderen bestehenden Geschäftsanwendungen.
* **Wird nicht haben** - Dein MVP sollte *keine* Funktionen haben, die nicht direkt zu Deinem Ziel beitragen. Beispiel: Rollenbasierte Zugriffskontrolle, mit der Unternehmen auswählen können, wie verschiedene Arten von Mitarbeitern mit den Rechnungen interagieren.

### Testen, lernen und anpassen
Ein MVP liefert wertvolles Feedback für die zukünftige Entwicklung. Es ist wichtig zu bedenken, dass ein MVP während seines Entwicklungszyklus verschiedene Änderungen erfordert. Du wirst später die Möglichkeit besitzen, alle Funktionen, die später benötigt werden, aus der Schublade zu implementieren. Der vollständige Funktionsumfang sollte nur unter Berücksichtigung der Rückmeldungen der ursprünglichen Benutzer entworfen und entwickelt werden. Beachte auch, dass **die Erkenntnisse aus einem MVP** möglicherweise weniger kostenintensiv ist als die Entwicklung eines Produkts mit mehr Funktionen.

Um die Funktionen für Dein MVP richtig zu definieren, solltest Du Folgendes beachten:
* Beginne mit dem "Warum?"
* Priorisiere Deine Funktionen
* Teste, lernen und passe an

Brauchst Du Hilfe bei der Definition der benötigten Funktionalitäten? [Buche einen Anruf](https://calendly.com/viper/kickstart/) mit uns. Du kannst auch gerne [3 Gründe warum du ein MVP erstellen solltest](https://viperdev.io/startup/2018/04/18/3-reasons-why-you-should-build-an-mvp) lesen. Wir würden uns über einen Kommentar und Gedankenaustausch mit Dir freuen.
