---
layout: post
title:  "VIPERdev Fördert Soziale Startups mit 10.000 Euro Softwareentwicklung"
date:   2018-10-29 17:48 +0100
author: "James Hiller"
profile_link: "https://www.linkedin.com/in/james-hiller-324734160"
categories: SME's
lang: de
image:
  feature: VIPERdev_Nikolauspitch.png
  credit: ""
---

Wirtschaftliche Startups haben es in unseren Breiten meist schon schwer genug, an eine Förderung durch öffentliche Gelder zu gelangen. Oder sogar an eine Seed-Finanzierung eines Investors. Soziale Startups haben es sogar noch einen kleinen Tick schwerer. Nicht wenige bedienen sich dabei Omas Sparstrumpf oder den eigenen Ersparnissen für ein ehrenwertes Ziel: Die Welt ein kleines Stückchen besser zu machen! Aus diesem Grund laden wir alle interessierten Social Startups dazu ein, sich bei uns für den [NikolausPITCH](https://nikolauspitch.de/) zu bewerben.

Zu gewinnen gibt es Preise zwischen 700 und 10.000 Euro. VIPERdev entwickelt zusammen mit dem Gewinner die Mobil- oder Webapp um dessen Vision umzusetzen. Die Preisverleihung findet statt am 6. Dezember (Nikolaustag) von 17 bis 19 Uhr im Startup Dock, Harburger Schloßstraße 6-12, 21079 Hamburg-Harburg. Die Bewerbungsfrist endet am 30. November. Um mitzumachen - als soziale Unternehmung oder neugieriger Teilnehmer - einfach <a href="https://NikolausPITCH.de">https://nikolauspitch.de</a> besuchen.

Wir bedanken uns für die multimediale Berichterstattung von Hamburg News und hoffen auf weitere Meldungen aus der Hamburg Medien- und Startup-Szene.

<a href="https://www.hamburg-news.hamburg/de/medien-it-kreativwirtschaft/nikolaus-pitch-foerderung-von-sozialen-startups" target="_blank">https://www.hamburg-news.hamburg/de/medien-it-kreativwirtschaft/nikolaus-pitch-foerderung-von-sozialen-startups</a>
