---
layout: post
title:  "Sind Fristen in der App-Entwicklung erforderlich?"
date:   2018-05-09 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: de
image:
  feature: time_for_business.png
  credit: "Freepik.com"
---

"Deadlines" sind ein zentraler Bestandteil des Projektmanagements, aber was ist der beste Weg, den Prozess beim Erstellen einer App zu planen? Trotz zahlreicher Vorteile können solche Fristen bei unsachgemäßer Verwendung zu einer Menge Stress führen. Berücksichtige daher diese Schritte, damit Dein Entwickler die gesetzte Frist einhalten kann:

### Setze realistische Erwartungen
In der Softwareentwicklung befinden sich einige Dinge außerhalb der Kontrolle des IT-Teams. Die mangelnde Beteiligung des Projektmanagers, häufige Aufgabenänderungen oder das Fehlen klarer Ziele sind nur ein paar Dinge, die Deinen Zeitplan beeinflussen können. Trotz dieser Faktoren muss der Eigentümer der App ein geschätztes Veröffentlichungsdatum wissen. Eine Lösung für die Festlegung einer realistischen Frist wäre es, **jedes Teammitglied zu bitten, Schätzungen** zu geben und dann je nach Komplexität der App ein paar Tage oder Wochen hinzuzufügen, um unerwartete Ereignisse abzudecken. Diese Methode kann den Entwicklern eine angemessene Zeit zur Verfügung stellen, **aber bedenke, dass lange Fristen nicht zum Arbeiten motivieren**, weshalb man das Projekt auch in kleinere Phasen aufteilen sollte.
### Identifizieren Sie die Meilensteine
Meilensteine sind kleine Schritte, die das Team zum Ziel führen: eine funktionierende App zu entwickeln. Versuchen Sie, **sie richtig zu platzieren**, indem Sie nicht in die Falle gehen, jede Aufgabe als Meilenstein zu kennzeichnen. Wenn Sie beispielsweise eine E-Commerce-App erstellen, kann Ihr Entwickler das Zieldatum für die Integration des Zahlungsprozesses kennen, muss jedoch nicht jedes Element der Benutzeroberfläche, das für eine Aktion erforderlich ist, als Meilenstein markieren, z Taste. Denken Sie daran, dass Entwickler es vorziehen, ihre Aktivitäten in Sprints zu organisieren, also versuchen Sie, aufgeschlossen zu sein, da mangelnde Flexibilität zu mangelnder Qualität führen kann. Ziehen Sie außerdem in Betracht, ein [Programm-Evaluierungs-Review-Technik-Diagramm](https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique) zu erstellen. PERT-Diagramme zeigen die Beziehung zwischen Projektmeilensteinen. Auf diese Weise weiß Ihr Entwickler, welche Meilensteine abgeschlossen sein müssen, bevor andere Aktivitäten gestartet werden oder welche Aufgaben parallel ausgeführt werden können.

### Verwende Feedback zur Anpassung
Besprechungen sind notwendig, um den Fortschritt zu überprüfen, **zeitnahes Feedback** zu geben und die Fragen Deiner Entwickler zu beantworten, aber sie kurz und produktiv zu halten. Erinnere ihn an die festgelegten Meilensteine, um auf seiner Seite zu bleiben. Wenn Dein Entwickler hinter dem Zeitplan zurückliegt, solltest Du unbedingt über die Ursachen sprechen und darüber, was verbessert oder vermieden werden kann. Wenn wichtige Deadlines aufgrund von Faktoren, die außerhalb Deiner Kontrolle liegen, übersehen werden, solltest Du die Möglichkeit in Erwägung ziehen, Überstunden zu bezahlen oder auf einige unwichtige Funktionen zu verzichten. Die Flexibilität **anzupassen und zu ändern**, wenn neue Herausforderungen auftreten, ist eine Schlüsselkomponente beim Aufbau einer erfolgreichen App.

Es ist zwar wichtig, Schätzungen vorzunehmen und Meilensteine mit Deinem App-Entwickler zu setzen, um effizienter zu arbeiten und keine Ressourcen zu verschwenden, aber es ist nicht immer ein reibungsloser Prozess. Egal, ob Du Termine bevorzugst oder die Aktivität in "Sprints" organisierst, stelle sicher, dass:
* realistische Erwartungen gesetzt werden
* die Meilensteine identifiziert werden
* Feedback zur Anpassung verwendet wird

Möchtest Du [Deine Geschäftsidee in ein Geschäftsmodell verwandeln?](https://viperdev.io/startup/2018/05/03/turn-your-business-idea-into-an-app) Fühle Dich frei einen [Anruf zu buchen](https://calendly.com/viper).
