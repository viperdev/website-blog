---
layout: post
title:  "Der Hauptgrund, warum Startups scheitern"
date:   2018-04-04 03:52:21 -0500
author: "Whitney Tennant"
profile_link: "https://www.linkedin.com/in/whitney-tennant-11a691151/"
categories: Startup
lang: de
image:
  feature: rocket-downwards.png
  credit: "Photoroyalty — Freepik.com"
---

Du beginnst mit Deinem neuen Projekt. Es könnte ein Startup sein oder eine bahnbrechende neue Anwendung, mit der Du einen neuen Markt erobern willst. Du bist ganz aufgeregt. Du willst anfangen. Du bist bereit, das Geld dafür auszugeben. Alles ist vorbereitet für Dein nächstes großes Ding.
### So what happens next?

Nachdem wir an vielen solchen Projekten gearbeitet hatten, haben wir gelernt, dass der nächste Schritt der schwierigste ist:

Der Hauptgrund, warum Startups und neue Produkte scheitern, ist vornehmlich [die Nicht-Befriedigung der Marktbedürfnisse](https://www.forbes.com/sites/groupthink/2016/03/02/top-20-reasons-why-startups-fail-infographic/).

Während der ersten Aufregung, die eine Geschäftsgründung mit sich bringt, ist es schwierig, schrittweise zu beginnen und den Markt nach und nach zu testen. Dies zu beachten, ist entscheidend und die gesamte Lösung besteht darin, Deine Idee schnell zu testen, bevor Du Geld in unnötige Elemente investierst.

Eine der schnellsten Methoden, um eine bestimmte Idee und Annahmen zu testen und anschließend zu bewerten, ist die Erstellung eines
 **“Minimum Viable Product” (MVP)**. Ein MVP ist ein "Setup", das nur die absolut notwendigen Funktionen abdeckt, um Dein Kernproblem zu lösen.

Nehmen wir zum Beispiel an, ich erfinde den ersten Online-Shop aller Zeiten. Das Problem, das ich löse, ist, dass die Leute viel zu viel Zeit damit verbringen, innerhalb der Geschäfte herumzulaufen, um Produkte zu bekommen, die sie mögen. Also stelle ich mir eine Website vor, auf der sie Artikel bestellen können, ohne ihr Haus zu verlassen.

Dies ist eine völlig neue Idee. Es gibt keine Konkurrenz, von der ich lernen kann und ich muss selbst meine Idee ausprobieren, um zu sehen ob sie funktioniert.

Ein MVP deckt hierbei nur die **unbedingt benötigten Features** ab. Es könnte eine statische Website sein, auf der ich die Produkte aufliste, von denen ich glaube, dass meine Kunden sie kaufen möchten, und unter jedem von ihnen stelle ich einen Link bereit, um mir eine E-Mail zu schicken. Die Benutzer würden mir einfach eine E-Mail senden und ich würde die Bezahlung mit ihnen individuell vereinbaren, danach in den Laden gehen, das Produkt für sie kaufen und es ihnen zusenden.

Mit diesem Setup kann ich meine Idee mit einer minimalen Investition testen. Wenn meine Nutzer das Konzept ignorieren, ist die Welt möglicherweise noch nicht bereit für den Online-Einkauf. Wenn sie es akzeptieren, kann ich meine App in Umlauf bringen und potenziellen Investoren sagen, dass diese Idee ein definitives Potenzial hat, **profitabel zu sein** — Außerdem bin ich bereits in der Lage dazu, meine Produktentwicklung basierend auf den Prioritäten meiner Nutzer anzupassen, weil ich ständig in Kontakt mit ihnen bin.

Es ist wichtig, Deine Ideen zu testen. In der Regel bezieht sich ein Startup an eine Lücke auf dem Markt, jedoch sehen Benutzer möglicherweise einige Aspekte als wertvoll an. Die beste Lösung ist daher die Bereitstellung von leichtgewichtigen Funktionen, die nach der Validierung erweitert werden können.

Der Hauptgrund für Fehler nach dem Unternehmensstart kann behoben werden. **Fehler vermeiden und früh testen**.

Wenn man beginnt, gibt es normalerweise kein Geld, das man verschwenden könnte. Es hat sich unserer Erfahrung nach als der beste Weg erwiesen, Fehler zu vermeiden. Dies können wir sagen, nachdem wir mit vielen Startups zusammengearbeitet haben, um mit ihnen gemeinsam die wesentlichen Punkte zu evaluieren und deren Kernwerte zu verbessern.

Wir freuen uns immer von Dir zu hören, lassen Sie uns wissen, was Du denkst und besuche uns auf [viperdev.io](https://viperdev.io/) oder schaue [hier](https://viperdev.io/startup/2017/12/14/the-beginning-of-an-mvp).
