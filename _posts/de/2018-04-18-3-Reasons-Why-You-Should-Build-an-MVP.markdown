---
layout: post
title:  "3 Gründe warum du ein MVP erstellen solltest"
date:   2018-04-18 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: de
image:
  feature: rocket_plain.png
  credit: "Freepik.com"
---

Mit einem Minimum Viable Product zu beginnen, ist eine großartige Möglichkeit, Dein Produkt schneller auf den Markt zu bringen, was Dir ein wertvolles Feedback gibt und es Dir ermöglicht, all dies mit einem knappen Budget zu tun. **Ein MVP sollte nur die Funktionen haben, die zur Erfüllung des grundlegenden Bedarfs erforderlich sind.** Natürlich gibt es keine einheitliche MVP-Strategie. Du musst Dich abhängig von der Branche, die Du wählst, dem Problem, das Du lösen möchtest, und dem bestehenden Wettbewerb anpassen.


### Der Aufbau eines erfolgreichen MVP ist der beste Weg, das gesamte Projekt zu finanzieren.

**Anleger** müssen normalerweise **Zugkraft** sehen, bevor sie bereit sind, ihr Geld einzuzahlen. Werfen wir einen Blick auf Airbnb. Alles begann im Jahr 2007, als Brian Chesky und Joe Gebbia beschlossen, ihr Wohnzimmer als billige Unterkunft für einige Design-Konferenzteilnehmer zu nutzen, die kein Glück in nahegelegenen Hotels hatten. Sie machten Fotos von ihrer Wohnung, luden sie auf eine einfache Website hoch und bald kamen 3 zahlende Gäste: eine Frau aus Boston, ein Mann aus Indien und ein Vater von vier Kindern aus Utah. Anfang 2009 erhielten sie 20.000 $ von Paul Graham, dem Mitbegründer von Y Combinator. Dies führte zu weiteren 600.000 $ von Risikokapitalgebern und der Rest ist Geschichte.


### Vermeide übermäßige Entwicklungskosten, indem Du die Dinge minimal hältst.

Entwicklungsunternehmen werden erfreut sein, eine lange Liste von abrechenbaren Stunden zu sammeln. Sogar Dein technischer Direktor wird höchstwahrscheinlich ansprechende, aber nicht essenzielle Merkmale kodieren - nicht mit schlechten Absichten, sondern weil es aufregender ist, etwas zu schaffen als herauszufinden, was der Markt will. Was bedeutet es, dass wir Dinge minimal halten wollen? Brian Chesky und Joe Gebbia, die Airbnb-Mitbegründer, schufen eine einfache Lösung, um die Nachfrage zu testen, indem sie Bilder auf einer einfachen Website zur richtigen Zeit - vor einer wichtigen Konferenz - hochluden. Sie schafften es, ihr Modell **zu überprüfen, ohne sich über Features Gedanken zu machen.** Als sie feststellten, dass Kunden nur die Wohnung und den Preis für den Vertragsabschluss sehen mussten, wussten sie, dass sie Investoren erreichen konnten.

### Erhalte Deine ersten zahlenden Kunden. Diese erste Iteration gibt Dir den finanziellen Auftrieb für die nächste Phase.

Ihre ersten Kunden werden Ihnen helfen, die nächsten Schritte nicht nur finanziell, sondern auch in Bezug auf Ihren möglichen **Zielmarkt** zu unternehmen. Es ist alles andere als einfach, sich an Early Adopters zu wenden, die Ihre erste Version verwenden möchten, vor allem, wenn Sie auf einen völlig unerschlossenen Markt zielen. Aus diesem Grund können Sie sich auf ein **Kundensegment** konzentrieren und diesen Kunden helfen, ihre Probleme zu lösen, als ob sie Ihre besten Freunde wären. So können Sie Ihr Produkt verbessern. Lassen Sie sie sich wertvoll fühlen, indem Sie nach ihrer Analyse und ihrem Feedback zu Produkt und Benutzererfahrung fragen. Der Prozess beinhaltet Messung, Lernen und Verbesserung, um eine starke Kundenbasis aufzubauen.

In seinem Brief an die Aktionäre im Jahr 2016 sagte Jeff Bezos folgendes:

>*Die meisten Entscheidungen sollten wahrscheinlich mit ungefähr 70 % der Informationen getroffen werden, die Sie wünschen. Wenn Sie auf 90 % warten, sind Sie in den meisten Fällen wahrscheinlich langsam. Außerdem müssen Sie in der Lage sein, schlechte Entscheidungen schnell zu erkennen und zu korrigieren. Wenn Sie gut im Korrigieren sind, ist es vielleicht weniger kostspielig, sich zu irren, als Sie denken, während es langsam teuer sein wird, langsam zu sein.*

Jeff Bezos Worte treffen auf jede Produktentwicklung in seinen frühen Stadien zu. Der Schlüssel ist, das ideale Bild, das Du über Dein Produkt haben wirst, von seinem **Kern-** Angebot zu trennen. Dies wird Dir dabei helfen, schneller auf den Markt zu kommen und **Deine Idee** mit einer geringeren Investition zu validieren.

**Behalte im Auge:**
1. Der Aufbau eines erfolgreichen MVP ist der beste Weg, das gesamte Projekt zu finanzieren.
2. Vermeide übermäßige Entwicklungskosten, indem Du die Dinge minimal hältst.
3. Hole Dir Deine ersten zahlenden Kunden. Diese erste Iteration gibt Dir den finanziellen Auftrieb für die nächste Phase.


Du könntest Dich auch hierfür interessieren: ["Der Hauptgrund, warum Startups scheitern"](https://viperdev.io/startup/2018/04/04/the-number-one-reason-why-startups-fail). Lasse uns an Deinen Gedanken teilhaben und besuche uns auf [viperdev.io](https://viperdev.io/).
