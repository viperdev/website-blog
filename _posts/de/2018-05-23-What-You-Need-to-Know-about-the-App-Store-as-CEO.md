---
layout: post
title:  "Was Du über den App Store als CEO wissen musst"
date:   2018-05-23 2:52:21 -0500
author: "Whitney Tennant"
profile_link: "https://www.linkedin.com/in/whitney-tennant-11a691151/"
categories: Startup
lang: de
image:
  feature: brain.png
  credit: "Freepik.com"
---

Als Inhaber eines Unternehmens musst Du an der Entwicklung Deiner App beteiligt sein. Es ist unerlässlich, die Gebühren zu kennen und das Konto Deines Unternehmens im App Store einzurichten.

Dies mag sich zwar wie eine Ablenkung von der Führung Deines Unternehmens anfühlen - ein wenig über die Entwicklung von Apps zu wissen, ist heutzutage nicht nur eine Notwendigkeit, sondern auch eine Schlüsselkompetenz für Unternehmer.

Während ich vielen Startups bei der Einführung von Apps mit [VIPERdev](https://viperdev.io/) geholfen habe, ist mir aufgefallen, wie seltsam es ist, unseren Kunden mitzuteilen, dass sie Entwicklerkonten mit Google, Apple oder Windows einrichten müssen.

Die Einrichtung eines Entwicklerkontos scheint etwas zu sein, das [wir](https://viperdev.io/) als App-Entwickler und -Berater bewerkstelligen könnten. Es gibt jedoch sehr spezifische Gründe, warum Dein App-Entwickler dieses Konto nicht für Dich erstellen kann. In diesem Beitrag wird erläutert, was Du als Unternehmensinhaber tun und bezahlen musst, um Deine App zu veröffentlichen.

### Erstellen eines plattformspezifischen Entwicklerkontos

Alle App Stores (Google, Apple und Windows) benötigen ein Entwicklerkonto, um Deine App zu veröffentlichen. Dadurch kannst Du auf das Entwicklerportal der jeweiligen Plattform zugreifen, Zahlungsoptionen festlegen sowie die Kommentare, Bewertungen (und vieles mehr) Deiner App einsehen. Sie ist mit diesem Entwicklerkonto verknüpft, welches von Dir, dem Geschäftsinhaber, und nicht von Deinem App-Entwickler erstellt wird.

Im Folgenden findest Du die jeweiligen Gebühren für jede Plattform mit Links für detailliertere Informationen zur erforderlichen Dokumentation. Wenn Du ein solches Konto erstellen möchtest, musst Du Deine privaten Dokumentations- und Zahlungsinformationen teilen. Dies kann nur von Dir - dem Geschäftsinhaber - geschehen.

Nach der Konto-Erstellung musst Du einen Administrator für Deinen App-Entwickler einrichten, um den Upload- und Validierungsprozess abzuschließen. Dein App-Entwickler benötigt für jede der Plattformen, mit denen Du Deine App starten möchtest, ein plattformspezifisches Entwicklerkonto.

**Google**
Erstelle Dein Entwickler-Konto [hier](https://play.google.com/apps/publish/signup/). Du musst eine einmalige Gebühr von 25 US-Dollar bezahlen. Die Verarbeitung und Registrierung kann bis zu 48 Stunden dauern.

**Apple**
Erstelle Dein Entwickler-Konto [hier](https://developer.apple.com/programs/enroll/). Dort siehst Du auch die Voraussetzungen für die Registrierung als Organisation. Das Ganze kostet 99 US-Dollar pro Jahr.

**Windows**
Erstelle Dein Entwickler-Konto [hier](https://developer.microsoft.com/en-us/store/register). Es kostet Dein Unternehmen einmalig rund 99 US-Dollar.

### Erstelle ein Admin-Account für Deinen Entwickler

Mit Google-, Apple- und Windows-Entwicklerkonten kannst Du außerdem verschiedene Personen zu unterschiedlichen Rollen hinzufügen. Solltest Du das Entwicklerkonto erstellen - Du wirst wahrscheinlich der Kontoinhaber sein - kannst du anschließend Administratoren hinzufügen, die über ausreichende Berechtigungen verfügen, um wichtige Aufgaben auszuführen. Sie sollten auf jeder Plattform einen Admin-Nutzer für Deinen App-Entwickler erstellen, damit dieser Deine App für Sie validieren und hochladen kann.


## Validierung und Hochladen Deiner Anwendung

Die Signierung ist ein Prozess, den alle App-Stores benötigen, und wird verwendet, um nachzuweisen, dass das mit Deinem Unternehmen verknüpfte Entwicklerkonto die Anwendung erstellt hat. Über Deine Entwicklerkonsole für jede Plattform kannst Du ein Zertifikat oder einen "Keystore" generieren, mit dem ein Entwickler den Anwendungscode signiert. Dein App-Entwickler kann dies für Dich tun, sobald er als Administrator auf dem Entwicklerportal der betreffenden Plattform hinzugefügt wurde. Nach der digitalen Signatur der App kannst du sie hochladen. Im Anschluss an den Upload wird die App überprüft. Die Bearbeitungszeit beträgt in der Regel maximal 48 Stunden.

### Launch timing
![Alarm Clock](/assets/img/blog/alarm_clock.png){:.img-feature}
Created by: Freepik.com

Stelle sicher, dass Du diese Konten eingerichtet hast, während Deine App entwickelt wird. Es kann eine Weile dauern, bis die erforderliche Dokumente gesammelt und nach dem Einreichen Deiner Dokumentation ausgewertet wurden. Denke daran, dass der Code vor dem Start Deiner App von Google, Apple oder Windows überprüft werden muss. Dies kann 48 Stunden oder länger dauern. Möglicherweise müss einige Funktionen geändert und neu eingereicht werden.

Du musst Dir auch darüber im Klaren sein, wie Du Deine Anwendung monetarisieren möchtest, da dies während der Upload- und Startphase festgelegt wird. Wir werden bald über die Monetarisierung Deiner App etwas schreiben. Informiere Dich regelmäßig in unserem [Blog](https://viperdev.io/blog/) über aufschlussreiche Artikel für Startups und die Entwicklung von Apps.
