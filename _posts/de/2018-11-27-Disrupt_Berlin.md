---
layout: post
title:  "VIPERdev bei der Disrupt Berlin 2018"
date:   2018-11-27 11:55 +0100
author: "Lolita Ljametova"
profile_link: ""
categories: Events
lang: DE
image:
  feature: hands-with-puzzles.png
  credit: "Freepik.com"
---
Am 29. und 30. November öffnet die [„Disrupt Berlin 2018“](https://techcrunch.com/events/disrupt-berlin-2018/#a28769d5-3890-46e1-a2b3-a487a3af295e) wieder ihre Türen. Das Online-Nachrichtenportal „TechCrunch“ lädt sämtliche StartUps und Unternehmer in die „Arena Berlin“ ein um sich an zwei Konferenztagen über die Neuigkeiten und Trends der Technologie-Branche auszutauschen.
In den Augen von TechCrunch ist jeder ein Startupper – ob nun Gründer, Investor oder auch Hacker, jeder ist willkommen.
Neben zahlreichen interessanten Vorträgen von Unternehmensgründern und prominenten Sprechern (wie z.B. Lucas Di Grassi), findet ein Pitch zwischen 15 Tech-Startups statt, welche im Wettkampf gegeneinander ein Preisgeld in Höhe von 50.000 Dollar gewinnen können.

Bei so einem Event darf unser Team von Viper Developement natürlich nicht fehlen. Am zweiten Konferenztag findet Ihr uns in der Startup Alley.
Als Startup für Softwareentwicklung bringen wir natürlich ein großes Interesse am Austausch mit anderen Gründern und am Networking mit, möchten aber natürlich auch die Wichtigkeit von MVP‘s an andere weitergeben.

Was MVP‘s sind fragt ihr euch? - Ganz einfach:

MVP steht für „Minimum Viable Product“, zu deutsch „minimal überlebensfähiges Produkt“, dies bedeutet ein Produkt mit minimalem Aufwand so schnell wie möglich an die richtige Kernzielgruppe und den richtigen Markt zu bringen. Und die Betonung liegt hierbei auf schnell und einfach.
Wir von VIPERdev kreieren binnen 30 Tagen die beste technische Lösung um die Bedürfnisse des Kunden zu finden und setzen diese gleichzeitig um.

Wenn ihr euch immer noch fragt wie das alles funktioniert und wie man ein solches Projekt startet, könnt ihr uns gerne einen Besuch bei der Disrupt Berlin abstatten und Informationen austauschen.
