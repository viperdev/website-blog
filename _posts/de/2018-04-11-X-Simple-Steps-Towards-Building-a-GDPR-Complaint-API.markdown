---
layout: post
title:  "Erstellung einer GDPR-kompatiblen API"
date:   2018-04-11 2:52:21 -0500
author: "Razvan Chitu"
profile_link: "https://www.linkedin.com/in/r%C4%83zvan-chi%C5%A3u-1bb891100/"
categories: Startup
lang: de
image:
  feature: gdpr.png
  credit: "Freepik.com"
---

**Haftungsausschluss: Dieser Artikel dient ausschließlich zu Informationszwecken und nicht zum Zweck der Rechtsberatung.**

### Für wen sind diese Ratschläge geeignet?

Wir erstellen eine Liste mit Vorschlägen für Entwickler von App-Backends und APIs. Wir würden uns freuen, die Liste auf Open-Source-Basis zu erweitern, also kannst Du gerne Deine Compliance-Tipps einreichen!

### Was ist die DSGVO und warum ist sie relevant für mein Backend?

Laut Wikipedia ist die Datenschutz-Grundverordnung (DSGVO) eine Verordnung des EU-Datenschutz- und Datenschutzgesetzes für alle Personen innerhalb der Europäischen Union. Es gilt für die Handhabung und Verarbeitung personenbezogener Daten von EU-Bürgern auf Online-Plattformen. Es räumt den Bürgern der EU neue Rechte ein, wie das Recht auf Vergessen oder das Recht auf Löschung. Du hast von diesen Begriffen wahrscheinlich kürzlich gehört.

Das Backend (oder die API) ist ein zentraler Bestandteil des Datenstomes einer App. Selbstverständlich musst du jetzt die DSGVO bei der Erstellung Deiner Applikation berücksichtigen. Hier ist unsere Liste von Schritten, die Dir dabei helfen, Deine Compliance zu verbessern.

### Persönliche Daten

Bei der DSGVO geht es um personenbezogene Daten: Name, E-Mail, Personalausweisnummer, Adresse, IP, Standort usw. - im Prinzip alles, was zur Identifizierung einer Person verwendet werden kann. Die ersten beiden Dinge, die Du tun solltest, ist zum einen zu identifizieren, welche personenbezogenen Daten du speicherstenm und wo diese verwendet werden. Während die Verwendung personenbezogener Daten von App zu App unterschiedlich ist, sind in der Regel einige wichtige Komponenten zu beachten:

   * Anmeldung
   * Authentifizierung / Login
   * Benutzer-Datenbank oder -Sammlung
   * Benutzerprofil
   * Account-Einstellungen
   * Sitzungsspeicher
   * Tracking-Software oder Dienste, die Du verwendest

Wahrscheinlich möchtest Du diese Code-Komponenten, die bei der Entwicklung des Projekts mit persönlichen Daten interagieren, im Auge behalten. Du kannst Kommentare einfügen, Deinen Code mit Anmerkungen versehen oder ein "compliance"-Tag in die Dokumentation Deiner Methode / Funktion / Route / Rückruffunktion einfügen. Dies wird es sehr einfach machen, die Verwendung von persönlichen Daten zu verfolgen. Du kannst sogar noch einen Schritt weiter gehen und ein Datenflussdiagramm erstellen, um zu erklären, was in Deinem System passiert - dies würde sowohl Deinem Team als auch den Aufsichtsbehörden helfen, die Einhaltung der DSGVO zu überprüfen.

Ein weiterer Vorteil wäre eine Testsuite, die Code abdeckt, der mit persönlichen Daten interagiert. Da personenbezogene Daten sehr wichtig sind, gilt das Gleiche für die Korrektheit des Codes, der sie verarbeitet. Du kannst sogar das oben erwähnte Annotationssystem verwenden, um Deine Berichterstattung einfach zu verfolgen!

### Zustimmung

Laut der Verordnung müssen Benutzer ihre Zustimmung zu den von einer Plattform ausgeführten Aktivitäten zur Verarbeitung personenbezogener Daten ausdrücklich erteilen. Darüber hinaus müssen Benutzer in der Lage sein, diese Aktivitäten jederzeit zu deaktivieren. Im Backend musst du nachverfolgen können, wer und wann seine Zustimmung gegeben hat. Verknüpfe Deine Benutzer-Datenbank oder -Sammlung zu einer Einwilligung. Das sollte hier Dein Problem lösen. Vergiss auch nicht, diese Zustimmungsflags zu überprüfen, bevor Du Deine Verarbeitungslogik ausführst!

Wenn Du nach einer Inspiration für ein System für die Benutzerzustimmung suchst, solltest Du versuchen, zu prüfen, wie Autorisierung und Berechtigung in gängigen Frameworks erfolgt. Diese Checkboxen für die Zustimmung sind nur eine vereinfachte Version davon.

### Rechte

Die DSGVO betont, dass die Nutzer das Recht haben müssen, alle persönlichen Daten, die Du über sie besitzst, zu löschen, zu berichtigen, darauf zuzugreifen und sie zu exportieren. Sie können Dich bitten, alle ihre Daten zu löschen, oder sie fragen Dich, ihren falsch geschriebenen Namen zu korrigieren. Wenn sie sich über Facebook eingeloggt und von dort ihre E-Mail erhalten haben, sollten sie das auch ändern können. Alle Daten, die Du über die Benutzer speicherst, sollten exportierbar sein für den Benutzer. Dieser [Artikel](https://techblog.bozho.net/gdpr-practical-guide-developers/){:target="_blank"}  schlägt ein mögliches Design für eine automatisierte Lösung vor und erklärt, warum Du vielleicht eines erstellen solltest.

Löschen, Bearbeiten und Exportieren **müssen jedoch nicht automatisiert werden**. Diese Dinge müssen nur zugänglich sein. Die einfachste und unkomplizierteste MVP-Lösung, die wir uns vorstellen können, ist ein Endpunkt, der diese Anforderungen einfach protokolliert oder speichert, um sie später manuell bearbeiten zu können. Es ist bei weitem nicht die beste Benutzererfahrung, aber wenn Du Teil eines kleinen Teams bist, ist es möglicherweise zu kompliziert, diese Anfragen vollständig zu automatisieren.

### Vorbeugung

Personenbezogene Daten müssen mit angemessener Sicherheit geschützt werden. Du kannst für den Verlust von Daten im Falle eines Verstoßes verantwortlich gemacht werden. Hier sind einige "Best Practices", die die Sicherheit Deines Systems verbessern:

  * Daten während der Übertragung verschlüsseln, auch wenn der Client und der Server sich auf demselben Computer befinden!
  * Daten im Ruhezustand verschlüsseln. Wenn Du einen Cloud-Anbieter verwendest, ist es wahrscheinlich bequem, auch seine Lösungen zu verwenden, um Deine Daten zu verschlüsseln und die Verschlüsselungsschlüssel zu verwalten. Wenn Deine Infrastruktur beispielsweise auf AWS ausgeführt wird, solltest Du einen Blick darauf werfen, was [hier](https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingEncryption.html){:target="_blank"} hier angeboten wird.
  * Beschränke den Zugriff auf Server, die Daten enthalten. Verwenden Sie sichere Kennwörter, verwenden Sie Berechtigungen für Benutzer und Gruppen, verwenden Sie ACLs usw.
  * Verwenden keine personenbezogenen Daten von Produktionsservern auf Entwicklungs- und Staging-Computern. Echte Benutzerdaten sollten einen Pseudonymisierungsprozess durchlaufen, bevor sie Testmaschinen erreichen.
  * Richte Firewalls ein! Lassen keinen offenen Zugriff auf Service-Ports zu, wenn dies nicht erforderlich ist. Dieser [Artikel](https://techcrunch.com/2018/03/02/the-worlds-largest-ddos-attack-took-github-offline-for-less-than-tens-minutes/){:target="_blank"} ist ein Beispiel dafür, was passieren kann, wenn Du diese Regel nicht befolgst.
  * Verwende keine veralteten Versionen von Software mit bekannten Sicherheitslücken.

### Prüfung

Du möchtest verfolgen, **wer auf welche** persönliche Daten zugreift und **wann** dies geschieht. Die natürliche Lösung ist hier, ein Logging-System einzurichten. Abhängig von Deinem Tech-Stack kannst Du dies auf Datenbankebene implementieren (einige Datenbanken haben eine integrierte Unterstützung dafür) oder auf den darüber liegenden Ebenen.

Stelle sicher, dass Du keine persönlichen Daten protokollierst. Wenn Du beispielsweise von Benutzern ausgegebene Anfragen protokollierst, sollten die Datensätze mit einer internen UID oder einem Hash-Tag anstelle der E-Mail-Adresse oder IP-Adresse des Benutzers gekennzeichnet werden. Auch nach dem Entfernen persönlicher Daten aus Protokollen enthalten sie immer noch wertvolle Informationen für einen potenziellen Angreifer. Du kannst dieses Risiko eliminieren, indem Du sie auch verschlüsselst.

### Die Zukunft dieses Beitrags

An dieser Stelle könntest Du Dich fragen, warum die Datenintegrität oder die Aufbewahrungsrichtlinien nicht erwähnt werden oder wir hier nicht eine "DSGVO-Klausel" einfügen. Wir freuen uns darauf, diesen Artikel mit mehr Informationen zu erweitern, aber gleichzeitig wollen wir es bei einer vernünftigen Länge belassen. Lasse es uns wissen, wenn Du denkst, wir sollten uns auf etwas Bestimmtes konzentrieren!
