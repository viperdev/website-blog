---
layout: post
title:  "NikolausPITCH - and the Winners Are..."
date: 2019-02-05 9:48 +0100
author: "Lolita Ljametova"
profile_link: "https://www.linkedin.com/in/lolita-ljametova-756238114/"
categories: Events
lang: en
image:
  feature: Nikolaus_Pitch_HEI_3512_Anne_Gaertner.jpg
  credit: "Anne Gaertner Photography"
---

On December 6th (St. Nicholas Day) the VIPERdev-Team invited social entrepreneurs to present their app idea to make the world a little bit better.
The basic idea was to help social founders with a digital solution by creating a social benefit.
Six finalists presented their ideas in front of an independent jury.


![Tobias Kästele](/assets/img/blog/Nikolaus_Pitch_HEI_3292_Anne_Gaertner.jpg){:.img-feature}
Created by: Anne Gaertner Photography

We welcomed Tobias Kästele and Christian Wiebe from "Viva con Agua" to our event, who introduced us to two independent social projects. Like most people know, the non-profit association "Viva con Agua de Sankt Pauli e.V." is committed to ensuring that everyone in the world has access to clean drinking water.

Kevin Fuchs also introduced us to his social project at the pitch. It's "Gexsi", a search engine whose search-generated revenue is invested in social projects.

![Kevin Fuchs](/assets/img/blog/Nikolaus_Pitch_HEI_3290_Anne_Gaertner.jpg){:.img-feature}
Created by: Anne Gaertner Photography

The jury was impressed by Christina Oskui, a children's book author who not only translated her works into Braille, she also dealt intensively with the topic of visual impairment and came across a problem that preoccupied blind and demented people. Christina Oskui wants to use a software solution to record photos with audio files in order to store the memories and make them accessible to people with disabilities or later generations. With this imaginative idea, she won the third price, a strategy consulting and software architecture planning worth € 700.

![Christina Oskui](/assets/img/blog/Nikolaus_Pitch_HEI_3455_Anne_Gaertner.jpg){:.img-feature}
Created by: Anne Gaertner Photography

Second place went to Enno Bassen for his interesting startup and network “curilab”. “curilab” is an app to simplify logging for nursing services with the help of technologies, leaving more time for interpersonal relationships. Protocols are made available to all users with access rights by means of a cloud protocol. The linguistic dialogue should be recorded with the help of artificial intelligence.
For his social project, Enno won a strategy consulting, software architecture planning and app design worth € 1,000,-.

![Enno Bassen](/assets/img/blog/Nikolaus_Pitch_HEI_3513_Anne_Gaertner.jpg){:.img-feature}
Created by: Anne Gaertner Photography


The first place went to Nadine Herbrich and Alessandro Cocco from "ReCycleHero". "ReCycleHero" is a pick-up service for waste glass, waste paper and returnable bottles. The glass is collected directly from the consumer and disposed of properly for him. The pickup is done by cargo bike, which also protects the environment. The “ReCycleHero” team employs mostly refugees, the long-term unemployed and the homeless, helping the financially disadvantaged in the integration into job and society.
So much social commitment and environmental awareness was rewarded by our jury with a software solution in the amount of € 10,000,-.

![Alessandro Cocco](/assets/img/blog/Nikolaus_Pitch_HEI_3500_Anne_Gaertner.jpg){:.img-feature}
Created by: Anne Gaertner Photography

At this point we would like to say thank you to our independent jury Katharina Osbelt, Dorothee Vogt and Christian Salzmann and of course to our event partner StartupDock.

We congratulate the winners and look forward to the next NikolausPITCH.
